#!/usr/bin/env python3

"""Interface to Sanity Checks"""

from functools import reduce, partial


def compose(*funcs: (callable)) -> callable:
    """Mathematically compose functions

    To be mathematically composable, the output of one
    function should be the input of the next. This is
    simplest to ensure if each function takes and returns
    the same output.

    Args:
        funcs: functions that are mathematically composable

    Returns:
        The composed functions awaiting their argument.

    """
    def compose2(acc, f):
        return f(acc)
    return partial(reduce, compose2, funcs)


class SanityCheckClient:
    """Interface to sanity checks"""

    def __init__(self, *funcs: (callable), method: callable=compose):
        """Set the testing method
        Args:
            funcs: all non-keyword args are expected
            to be the functions used in the tests.
        KWargs:
            method: default is compose, could potentially be preplaced with
            something concurrent if the number of tests grows.
        """
        self.tests = method(*funcs)

    def process(self, prior: dict, row: dict):
        """Check if all tests are positive

        To Do:
            Currently the first row is not fully checked

        Args:
            prior: the dict for the previous row for context
            row: the dict for the row being processed

        """
        rows = prior, row
        self.tests(rows)

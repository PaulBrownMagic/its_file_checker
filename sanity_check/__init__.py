#!/usr/bin/env python3

"""Checks to ensure data makes sense in relation to context

   Functional composition variation on strategy design pattern

   ToDo: SanityCheckClient.process, check first row
"""


__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.2.1"
__date__ = "2018-08-01"

from .client import SanityCheckClient

#!/usr/bin/env python3

"""Test Cases for sanity checks

These are utilised by the SanityCheckClient
"""


from datetime import datetime, timezone
from functools import wraps


def test_case(test: callable):
    """Decorator to ensure args are returned for composition
    and to handle tests that return False"""
    @wraps(test)
    def wrapper(args, **kwargs):
        if not test(*args):
            raise ValueError(f"{test.__name__} failed, {test.__doc__}")
        return args
    return wrapper


@test_case
def sequential_ids(prior: dict, row: dict) -> bool:
        """The id number should be greater than the prior"""
        return prior['id'] < row['id']


@test_case
def recent_report(_, row: dict) -> bool:
    """The row time stamp should be within the last day"""
    td = datetime.now(timezone.utc) - row['date_time'].value
    return td.days == 0  # within 24 hours prior to now

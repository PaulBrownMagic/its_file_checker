#!/usr/bin/env python3

"""Write out processed data"""

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.0.1"
__date__ = "2018-08-07"

from csv import DictWriter
from json import dumps, dump


def csv_stdout(rows: [dict]) -> None:
    """Output the data as csv to stdout

    Args:
        rows: the checked data
    """
    def csv(iterable):
        """Output one row in csv format"""
        print(", ".join(map(str, iterable)))

    # Header row
    csv(rows[0].keys())

    # Data rows
    for row in rows:
        csv(row.values())


def csv_file(rows: [dict], filename: str) -> None:
    """Output the data as csv to filename

    Args:
        rows: the checked data
    """
    with open(filename, 'w') as csvfile:
        fieldnames = rows[0].keys()  # initial order retained
        writer = DictWriter(csvfile, fieldnames=fieldnames)

        # Header row
        writer.writeheader()

        # Data rows
        for row in rows:
            writer.writerow(row)


def json_stdout(rows: [dict]) -> None:
    """Output the data as json to stdout

    Args:
        rows: the checked data
    """
    # default=str to serialise custom types and print their original strings
    print(dumps(rows, default=str, indent=2))


def json_file(rows: [dict], filename: str) -> None:
    """Output the data as json to filename

    Args:
        rows: the checked data
     """
    with open(filename, 'w') as jsonfile:
        # default=str to serialise custom types and print original strings
        dump(rows, jsonfile, default=str, indent=2)


class OutputClient:
    """Interface to choose the output format and destination of the data"""
    formats = {("csv", "stdout"): csv_stdout,
               ("csv", "file"): csv_file,
               ("json", "stdout"): json_stdout,
               ("json", "file"): json_file
               }

    def __init__(self, output_format: str="csv", filename: str=None) -> None:
        """Set the format and destination"""
        if filename is None:
            self.writer = self.formats[(output_format, "stdout")]
        else:
            self.writer = self.formats[(output_format, "file")]
        self.filename = filename

    def write(self, rows: [dict]) -> None:
        """Write out the data"""
        if self.filename is None:
            self.writer(rows)
        else:
            self.writer(rows, self.filename)

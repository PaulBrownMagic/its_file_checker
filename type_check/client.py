#!/usr/bin/env python3

"""Client to type checking interface"""

from .typefactories import (AllowEmpty,
                            SomeEmpty,
                            Strict)


class TypeCheckClient:
    """Type checker that can process rows of input file

    Interface to type checking, can set parameters to determine
    factory to use when processing a row.
    """

    def __init__(self, allow_empty: str="no") -> None:
        """Initialise type checking client

        Args:
            allow_empty: flag to permit empty field values
        """

        if allow_empty == "any":
            self.customtypefactory = AllowEmpty()
        elif allow_empty == "some":
            self.customtypefactory = SomeEmpty()
        else:
            self.customtypefactory = Strict()

    def process(self, row: dict) -> dict:
        """Type check a row by transforming values to instances of custom types

        Args:
            row: a dictionary of format: {field_header: value}

        Returns:
            row with values as instances of appropriate custom type

        Raises:
            exceptions raised by type checking are not caught
        """
        return {ct: self.customtypefactory.create(ct, v) for ct, v
                in row.items()}

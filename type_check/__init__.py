#!/usr/bin/env python3

"""type_check:  Ensures inputs are of the expected custom types.

TypeCheckClient in .client provides an interface to a factory
that will instantiate a field value to the custom type determined
by a dict of types in the factory.

Custom types and their restrictions are defined in .customtypes.
"""

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.2.1"
__date__ = "2018-07-29"

from .client import TypeCheckClient

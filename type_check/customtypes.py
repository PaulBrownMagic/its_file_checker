#!/usr/bin/env python3

"""Defined Custom Types for checking"""

from re import compile, fullmatch
from datetime import datetime


class CustomType:
    """Interface ensuring values are of the expected type

    Checks the provided value is of the expected type during initialisation.
    It then converts the provided value from a string to a python type
    that can be used in comparisons.

    Attributes:
        original_value (str): the value passed in
        value (str): the converted value, in this case still a string
    """

    pythontype: callable = None  # used for the conversion if provided

    def __init__(self, value: str) -> None:
        """Initialisation of Custom Type

        Runs the type check, stores the value, and calls the conversion.

        Args:
            value: an atom of data from an input row
        """
        self.check(value)
        self.original_value: str = value
        self.convert()

    def convert(self) -> None:
        """Convert the original_value to a python datatype

        Only works if pythontype has been set to a callable function that
        performs the conversion. For example, if pythontype = int, then
        this will assign self.value to int(self.original_value).

        If pythontype has not been set, this will set self.value to the
        original_value string, assuming that no conversion was necessary.
        This is done to ensure uniformity across all custom types."""
        if self.pythontype is not None:
            self.value = self.pythontype(self.original_value)
        else:
            self.value = self.original_value

    def check(self, value: str) -> None:
        """Checks if the value is of an appropriate type.

        For CustomType, which is a base class, this does nothing.

        Args:
            value: an atom of data from an input row
        """
        pass

    def __str__(self) -> str:
        """Return the original string provided"""
        return self.original_value

    def __eq__(self, other) -> bool:
        """Equality comparison between custom types"""
        return self.value == other.value

    def __lt__(self, other):
        """Less than, and thus greater than comparison between custom types"""
        return self.value < other.value

    def __le__(self, other):
        """Less than or equal to, plus greater than or equal to comparison"""
        return self.value <= other.value

    def __repr__(self):
        """Instance description"""
        return f"<{self.__class__.__name__}: {self.original_value}>"


class Empty(CustomType):
    """Empty describes a data atom that held nothing, was null or empty"""
    pythontype = lambda *_: None  # function that returns None

    def check(self, value: str) -> None:
        """A value is determined to be empty if the length of the string is 0

        Args:
            value: an atom of data from an input row

        Raises:
            ValueError if the value is not empty
        """
        if not len(value) == 0:
            raise ValueError(f"{value} is not empty")
        super().check(value)


class String(CustomType):
    """String describes an atom of data that should be treated as text"""
    pass


class RegExpType(CustomType):
    """RegExpType is a custom type that is identified through regex

    These types are the numerical ones and datetime. They require a
    regex for identification and are converted to python types through
    the convert function"""
    regex = None  # The regex for identification
    type_name = ""  # A readable type description used in error messages

    def check(self, value: str) -> None:
        """check the value appears to be of the correct type

        Uses the regex to ensure the string value appears to be
        of the correct type. It requires a regex to be set.

        Args:
            value: an atom of data from an input row

        Raises:
            NotImplemented: If regex is not set
            ValueError: If regex doesn't match completely
        """
        if self.regex is None:
            err_msg = f"Missing Regex for custom type: {self.type_name}"
            raise NotImplementedError(err_msg)
        # else
        if not fullmatch(self.regex, value):
            raise ValueError(f"'{value}' is not a {self.type_name}")
        # else
        super().check(value)


class Natural(RegExpType):
    """A natural number, eg. 0, 1, 2, 3 .. 999, 1000 etc."""
    pythontype = int  # convert to an int
    regex = compile(r"\d+")  # one or more numerical digits
    type_name = "natural number"


class RealPlus(RegExpType):
    """A member of the positive real numbers, including 0.0, 1.0, 3.14 etc."""
    pythontype = float  # convert to a float
    regex = compile(r"\d+\.\d+")  # digit(s).digit(s)
    type_name = "positive real number"


class DateTime(RegExpType):
    """Date and Time with timezone offset information"""
    regex = compile(r"\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d\+\d\d:\d\d")
    type_name = "ISO-standard date time format with timezone offset"

    def convert(self):
        """Convert string value to python datetime.datetime object

        Overwrites parent class method due to additional complexity.
        """
        # reformat original value for use in strptime (remove : from tzinfo)
        reformatted = self.original_value[:-3] + self.original_value[-2:]
        self.value = datetime.strptime(reformatted, "%Y-%m-%dT%H:%M:%S%z")


class Speed(RealPlus):
    """Speed, km/h as per data description, represented as a positive real"""
    pythontype = float  # convert to a float
    max_speed = 432  # km/h, 432 current world record for road legal vehicle

    def check(self, value: str):
        """Add additional check that the speed value is less than max_speed

        Calls parent method first, which may raise errors if value is not
        a positive real number.

        Args:
            value: an atom of data from an input row

        Raises:
            ValueError: if speed is faster than max_speed
        """
        super().check(value)
        if float(value) > self.max_speed:
            raise ValueError(f"{value}km/h is faster than world record")

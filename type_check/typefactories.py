#!/usr/bin/env python3

"""Factories to type check via custom type instantiation"""

from .customtypes import (CustomType,
                          DateTime,
                          Empty,
                          Natural,
                          RealPlus,
                          Speed,
                          String)


class CustomTypeFactory:
    """Factory for custom types, creates appropriate custom type classes

    Attributes:
        types: a dictionary of input file header row names as keys. The values
               are the expected custom type for that field.
    """
    types = dict(id=Natural,
                 date_time=DateTime,
                 x=RealPlus,
                 y=RealPlus,
                 z=RealPlus,
                 segment=RealPlus,
                 length_m=RealPlus,
                 length_s=RealPlus,
                 street_category=String,
                 calculate_speed_kmh=Speed,
                 incident_start_x=RealPlus,
                 incident_start_y=RealPlus,
                 incident_length=RealPlus,
                 incident_delay=RealPlus,  # Actually empty in sample data
                 incident_absolute_speed=Speed,
                 incident_message=String
                 )

    def create(self, customtype: str, value: str) -> None:
        """Create an appropriate type-checking custom type class

        Base class, therefore not implemented.

        Raises:
            NotImplemented: this should be overwritten in subclass.
        """
        raise NotImplemented("create method undefined")


class Strict(CustomTypeFactory):
    """Each value must exactly match the expected custom type"""

    def create(self, customtype: str, value: str) -> CustomType:
        """Create an appropriate type-checking custom type class

        Makes use of the types dictionary to determine appropriate type class
        for the given field.

        Args:
            customtype: header row field name
            value: an atom of data from an input row expected to belong to
                   provided field

        Returns:
            Class instance of the appropriate custom type
        """
        return self.types[customtype](value)


class AllowEmpty(CustomTypeFactory):
    """Each value must be empty or exactly match the expected custom type"""

    def create(self, customtype: str, value: str) -> CustomType:
        """Create an appropriate type-checking custom type class

        Makes use of the types dictionary to determine appropriate type class
        for the given field. Checks to see if field is empty before type
        checking for provided type.

        Args:
            customtype: header row field name
            value: an atom of data from an input row expected to belong to
                   provided field

        Returns:
            Class instance of the appropriate custom type
        """
        try:
            return Empty(value)
        except ValueError:
            return self.types[customtype](value)


class SomeEmpty(CustomTypeFactory):
    """Each value must be empty or exactly match the expected custom type"""

    allow_empty = dict(id=False,
                       date_time=False,
                       x=False,
                       y=False,
                       z=False,
                       segment=False,
                       length_m=False,
                       length_s=False,
                       street_category=False,
                       calculate_speed_kmh=False,
                       incident_start_x=False,
                       incident_start_y=False,
                       incident_length=False,
                       incident_delay=True,
                       incident_absolute_speed=False,
                       incident_message=False
                       )

    def create(self, customtype: str, value: str) -> CustomType:
        """Create an appropriate type-checking custom type class

        Makes use of the types dictionary to determine appropriate type class
        for the given field. Checks to see if field is allowed to be empty
        and if it is empty before type checking for provided type.

        Args:
            customtype: header row field name
            value: an atom of data from an input row expected to belong to
                   provided field

        Returns:
            Class instance of the appropriate custom type
        """
        if self.allow_empty[customtype]:
            try:
                return Empty(value)
            except ValueError:
                pass  # Not empty, test against expected type
        return self.types[customtype](value)

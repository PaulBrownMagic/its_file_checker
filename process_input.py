#!/usr/bin/env python3

"""Loading input files ready for processing"""

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.0.2"
__date__ = "2018-08-01"

from csv import DictReader


def generate_input_rows(input_file: str) -> "generator(dict)":
    """Read data in from the input file

    Expects a csv file with a header row, no clever checking
    implemented yet.

    Args:
        input_file: input file name with path

    Returns:
        generator yielding dictionaries describing rows
    """
    def check(field_header):
        """'id' field header is empty, replace it with 'id'"""
        if field_header == '':
            return "id"
        return field_header

    with open(input_file) as csvfile:
        reader = DictReader(csvfile, restkey="id")
        for row in reader:
            yield {check(k): v for k, v in row.items()}


def list_input_rows(input_file: str) -> [dict]:
    """Read in from the input file

    Uses generate_input rows to create a list for
    non-lazy evaluation.

    Args:
        input_file: input file name with path

    Returns:
        list of dictionaries describing rows
    """
    return list(generate_input_rows(input_file))

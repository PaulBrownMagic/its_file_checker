import unittest

from type_check.customtypes import (Natural,
                                    RealPlus,
                                    Speed,
                                    DateTime,
                                    Empty,
                                    String)
from type_check.typefactories import AllowEmpty, Strict, SomeEmpty
from type_check.client import TypeCheckClient

from datetime import timezone


class TestEmpty(unittest.TestCase):

    def test_empty(self):
        self.assertTrue(Empty(""))

    def test_empty_value(self):
        self.assertEqual(Empty("").value, None)

    def test_not_empty(self):
        with self.assertRaises(ValueError):
            Empty("1")
            Empty("a")
            Empty(" ")


class TestString(unittest.TestCase):

    def test_string(self):
        self.assertTrue(String("abc"))

    def test_string_value(self):
        self.assertEqual(String("abc").value, "abc")


class TestNatural(unittest.TestCase):

    def test_natural(self):
        self.assertTrue(Natural("1"))

    def test_natural_value(self):
        self.assertEqual(Natural("0").value, 0)
        self.assertEqual(type(Natural("1").value), type(1))

    def test_not_natural(self):
        with self.assertRaises(ValueError):
            Natural("1.2")
            Natural("abc")
            Natural("")


class TestRealPlus(unittest.TestCase):

    def test_realplus(self):
        self.assertTrue(RealPlus("1.2"))

    def test_realplus_value(self):
        self.assertEqual(type(RealPlus("0.0").value), type(0.0))
        self.assertEqual(RealPlus("3.14").value, 3.14)

    def test_not_realplus(self):
        with self.assertRaises(ValueError):
            RealPlus("1")
            RealPlus("1.")
            RealPlus(".2")
            RealPlus("abc")
            RealPlus("")


class TestDateTime(unittest.TestCase):

    def test_datetime(self):
        self.assertTrue(DateTime("2018-07-29T23:36:14+00:00"))

    def test_datetime_value(self):
        dt = DateTime("2018-07-29T23:36:14+00:00")
        self.assertEqual(dt.value.year, 2018)
        self.assertEqual(dt.value.month, 7)
        self.assertEqual(dt.value.day, 29)
        self.assertEqual(dt.value.hour, 23)
        self.assertEqual(dt.value.minute, 36)
        self.assertEqual(dt.value.second, 14)
        self.assertEqual(dt.value.tzinfo, timezone.utc)

    def test_not_datetime(self):
        with self.assertRaises(ValueError):
            DateTime("2018-07-29")
            DateTime("")


class TestSpeed(unittest.TestCase):

    def test_speed(self):
        self.assertTrue(Speed("50.0"))

    def test_speed_value(self):
        self.assertEqual(type(Speed("65.2").value), type(65.2))
        self.assertEqual(Speed("12.6").value, 12.6)

    def test_not_speed(self):
        with self.assertRaises(ValueError):
            Speed("999.99")
            Speed("1")
            Speed("")
            Speed("a")


class TestAllowEmpty(unittest.TestCase):

    def test_create(self):
        row = dict(id="599557",
                   date_time="2017-12-23T08:00:00+00:00",
                   x="14.075815729",
                   y="",
                   z="97.339",
                   segment="1861.0",
                   length_m="325909.0",
                   length_s="",
                   street_category="MOTORWAY",
                   calculate_speed_kmh="56.0",
                   incident_start_x="4486965.0",
                   incident_start_y="5251429.0",
                   incident_length="5377.0",
                   incident_delay="",
                   incident_absolute_speed="60.0",
                   incident_message="A14/E55 northwestern Autostrada" +
                                    "Adriatica at Silvi roadworks"
                   )
        allowempty = AllowEmpty()
        for customtype, value in row.items():
            if value == "":
                t = Empty
            else:
                t = AllowEmpty.types[customtype]
            self.assertEqual(t, type(allowempty.create(customtype, value)))


class TestSomeEmpty(unittest.TestCase):

    def test_create(self):
        row = dict(id="599557",
                   date_time="2017-12-23T08:00:00+00:00",
                   x="14.075815729",
                   y="42.576935608",
                   z="97.339",
                   segment="1861.0",
                   length_m="325909.0",
                   length_s="16421.0",
                   street_category="MOTORWAY",
                   calculate_speed_kmh="56.0",
                   incident_start_x="4486965.0",
                   incident_start_y="5251429.0",
                   incident_length="5377.0",
                   incident_delay="",
                   incident_absolute_speed="60.0",
                   incident_message="A14/E55 northwestern Autostrada" +
                                    "Adriatica at Silvi roadworks"
                   )
        someempty = SomeEmpty()
        for customtype, value in row.items():
            if value == "":
                t = Empty
            else:
                t = SomeEmpty.types[customtype]
            self.assertEqual(t, type(someempty.create(customtype, value)))


class TestStrict(unittest.TestCase):

    def test_create(self):
        row = dict(id="599557",
                   date_time="2017-12-23T08:00:00+00:00",
                   x="14.075815729",
                   y="42.576935608",
                   z="97.339",
                   segment="1861.0",
                   length_m="325909.0",
                   length_s="16421.0",
                   street_category="MOTORWAY",
                   calculate_speed_kmh="56.0",
                   incident_start_x="4486965.0",
                   incident_start_y="5251429.0",
                   incident_length="5377.0",
                   incident_delay="0.0",
                   incident_absolute_speed="60.0",
                   incident_message="A14/E55 northwestern Autostrada" +
                                    "Adriatica at Silvi roadworks"
                   )
        strict = Strict()
        for customtype, value in row.items():
            self.assertEqual(Strict.types[customtype],
                             type(strict.create(customtype, value))
                             )


class TestTypeCheckClient(unittest.TestCase):

    def test_factory_maker(self):
        strict = TypeCheckClient(allow_empty="no")
        allowempty = TypeCheckClient(allow_empty="any")
        someempty = TypeCheckClient(allow_empty="some")
        self.assertEqual(type(strict.customtypefactory), Strict)
        self.assertEqual(type(allowempty.customtypefactory), AllowEmpty)
        self.assertEqual(type(someempty.customtypefactory), SomeEmpty)

    def test_process_row(self):
        row = dict(id="599557",
                   date_time="2017-12-23T08:00:00+00:00",
                   x="14.075815729",
                   y="42.576935608",
                   z="97.339",
                   segment="1861.0",
                   length_m="325909.0",
                   length_s="16421.0",
                   street_category="MOTORWAY",
                   calculate_speed_kmh="56.0",
                   incident_start_x="4486965.0",
                   incident_start_y="5251429.0",
                   incident_length="5377.0",
                   incident_delay="",
                   incident_absolute_speed="60.0",
                   incident_message="A14/E55 northwestern Autostrada" +
                                    "Adriatica at Silvi roadworks"
                   )
        checker = TypeCheckClient(allow_empty="some")
        checked = checker.process(row)
        for k, v in checker.customtypefactory.types.items():
            if row[k] == "":
                t = Empty
            else:
                t = v
            self.assertEqual(type(checked[k]), t)

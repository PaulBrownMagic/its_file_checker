import unittest

from sanity_check.checks import (sequential_ids,
                                 recent_report)
from sanity_check.client import SanityCheckClient, compose

from type_check.customtypes import DateTime

from datetime import datetime, timezone


class TestSequentialIds(unittest.TestCase):

    def test_sequential_ids(self):
        prior = dict(id=1)
        row = dict(id=2)
        correct = (prior, row)
        self.assertTrue(sequential_ids(correct))

    def test_not_sequential_ids(self):
        prior = dict(id=3)
        row = dict(id=2)
        incorrect = (prior, row)
        with self.assertRaises(ValueError):
            self.assertFalse(sequential_ids(incorrect))


class TestRecentReport(unittest.TestCase):
    now = DateTime(datetime.now(timezone.utc).isoformat(timespec='seconds'))

    def test_recent_report(self):
        row = dict(date_time=self.now)
        args = (row, row)
        self.assertTrue(recent_report(args))

    def test_not_recent_report(self):
        row = dict(date_time=DateTime('2017-08-01T14:43:47+00:00'))
        args = (row, row)
        with self.assertRaises(ValueError):
            self.assertFalse(recent_report(args))


class TestCompose(unittest.TestCase):

    def test_positive(self):
        incr = lambda x: x + 1
        incrincr = compose(incr, incr)
        self.assertEqual(incrincr(1), incr(incr(1)))


class TestSanityCheckClient(unittest.TestCase):

    def test_init(self):
        import dis  # check byte code is the same ...
        sc = SanityCheckClient(sequential_ids, recent_report, method=compose)
        funcs = compose(sequential_ids, recent_report)
        self.assertEqual(dis.dis(sc.tests), dis.dis(funcs))

    def test_process(self):
        prior = dict(id=1)
        row = dict(id=2)
        sc = SanityCheckClient(sequential_ids)
        sc.process(prior, row)

    def test_neg_process(self):
        prior = dict(id=3)
        row = dict(id=2)
        sc = SanityCheckClient(sequential_ids)
        with self.assertRaises(ValueError):
            sc.process(prior, row)

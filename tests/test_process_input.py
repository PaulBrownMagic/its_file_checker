import os
import unittest

from process_input import (generate_input_rows,
                           list_input_rows)

INPUT_FILE = os.path.join("tests", "test_data.csv")
HEADERS = ["id",
           "date_time",
           "x",
           "y",
           "z",
           "segment",
           "length_m",
           "length_s",
           "street_category",
           "calculate_speed_kmh",
           "incident_start_x",
           "incident_start_y",
           "incident_length",
           "incident_delay",
           "incident_absolute_speed",
           "incident_message"]


class TestGenerateInputRows(unittest.TestCase):
    rows = list(generate_input_rows(INPUT_FILE))

    def test_proportions(self):
        self.assertEqual(len(self.rows), 4)
        self.assertEqual(len(self.rows[1]), 16)

    def test_headers(self):
        for h, d in zip(HEADERS, self.rows[0]):
            self.assertEqual(h, d)


class TestListInputRows(unittest.TestCase):

    def test_compare_to_gen(self):
        self.assertEqual(list(generate_input_rows(INPUT_FILE)),
                         list_input_rows(INPUT_FILE))

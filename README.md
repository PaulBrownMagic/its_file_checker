# Program Design

input file -> checking -> output file

input file:
  1. split maybe(header) from rows

checking:
  0.  check header row
  1.  map type checking over rows to build typed rows
  2.  sanity checks between rows

output file:
  0. write out header
  1. write out all valid rows

? Error strategy ?

checking strategy:
  *  use header row for data types
  *  use factory to type check against header data types
  *  use config/flags for user to allow empty fields
  *  convert strings to builtins for sanity checks
  *  user prior and next row for sanity checks

type checks:
  *  positive int/float, str, or date
  *  range or length
  *  street_category may be enumerable
  *  some values may be checkable through calculations according to
     description

sanity checks:
  *  date makes sense
  *  id is incremental
  *  with enough data, even if not calculable, could learn
     unlikely relationships between appropriate variables,
     seems like overkill.

## Type Checking Classes
(rabbit-esq syntax)

CustomType is a Class which,
   overrides __ methods for rich comparison with python type
   overrides \_\_str\_\_ to return original val
   has method check: pass
   has method write: output original value for output file

Natural (ℕ₀) is a kind of CustomType which,
     has regex for positive integer
     converts val to int

RealPlus (ℝ₊) is a kind of CustomType which,
    has regex for positive float, must include decimal point and at
    least one digit after it.
    converts val to float

DateTime is a kind of CustomType which,
    has regex for ISO standard date, time and time offset from UTC
    converts val to Python datetime object

Empty is a kind of CustomType which,
    if len(val) == 0, then it is empty
    converts val to None

String is a kind of CustomType which
    is a fallback value, no check as original value is str
    leaves val as str

Coordinate is a kind of RealPlus.

Speed is a kind of RealPlus which,
    has max value 432km/h (Bugatti Veyron Super Sport with speed
                           limiter deactivated, fastest recorded
                           speed of road legal vehicle)

CustomTypeFactory is a Class which,
    has classmethod create which,
         takes a type as per the file header,
         returns a class instance of the appropriate type

## Type Checking Error Handling

In CustomType check method, if making type fails, raises error.

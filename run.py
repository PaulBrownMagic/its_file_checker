#!/usr/bin/env python3

""" Example usage """

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.0.2"
__date__ = "2018-08-01"

from process_input import generate_input_rows
from type_check import TypeCheckClient
from sanity_check import SanityCheckClient
from sanity_check.checks import sequential_ids, recent_report
from create_output import OutputClient

# Load into Type Checker
tc = TypeCheckClient(allow_empty="any")
typed_data = list(map(tc.process, generate_input_rows("tests/test_data.csv")))
print("Type checking complete")

# Sanity Check
sc = SanityCheckClient(sequential_ids, recent_report)
for prior, row in zip(typed_data, typed_data[1:]):
    sc.process(prior, row)
print("Sanity checking complete")

# Output
print("Output", end="\n\n")
writer = OutputClient(output_format="json", filename="del.json")
writer.write(typed_data)

�	date_time: timestamp of the request
�	x, y, z: route point coordinates (wgs84)
�	segment: number of the segment (at route level) which includes the point
�	length_m: accumulated segments length in meters (is the sum to previous values)
�	length_s: accumulated segments length in seconds (is the sum to previous values)
�	street_category: highway, freeways, trunk roads, country roads, residential etc
�	calculated_speed_kmh: the travel time at segment level used for route calculation
�	incident_start_x, incident_start_y: coordinates of the incident
�	incident_length: the affected length of the incident (meters)
�	incident_delay: the delay caused by the incident (seconds)
�	incident_absolute_speed: current absolute speed on the road segments.
�	message: description of the incident